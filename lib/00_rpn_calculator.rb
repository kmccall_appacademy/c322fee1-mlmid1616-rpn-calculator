class RPNCalculator
  attr_accessor :calculator

  def initialize
    @stack = []
    @length = 0
  end

  def push(num)
    @stack << num
    @length += 1
  end

  def plus
    self.checker
    popped1 = @stack.pop
    popped2 = @stack.pop
    @stack << popped1 + popped2
    @length -= 1
  end

  def value
    @stack[-1]
  end


  def minus
    self.checker
    ultimate_num = @stack.pop
    pen_ult_num = @stack.pop
    @stack << pen_ult_num - ultimate_num
    @length -= 1
  end

  def divide
    self.checker
    ultimate_num = @stack.pop
    pen_ult_num = @stack.pop
    @stack << pen_ult_num.fdiv(ultimate_num)
    @length -= 1
  end

  def times
    self.checker
    ultimate_num = @stack.pop
    pen_ult_num = @stack.pop
    @stack << ultimate_num * pen_ult_num
    @length -= 1
  end

  def tokens(tokens)
    operations = ["+","-","*","/"]
    tokens.split.map do |token|
      if operations.include?(token)
        token.to_sym
      else
        token.to_i
      end
    end
  end

  def evaluate(str)
      self.tokens(str).each do |item|
        if item == :+
          self.plus
        elsif item == :-
          self.minus
        elsif item == :*
          self.times
        elsif item == :/
          self.divide
        elsif item.class == Fixnum
          self.push(item)
        end
      end
      self.value
    end


    def checker
      if @length <= 1
        raise "calculator is empty"
      end
    end

end

# # class RPNCalculator
# #
# #   def initialize
# #       @stack = []
# #       @val = nil
# #   end
# #
# #
# #   def push(arg)
# #       @stack << arg
# #   end
# #
# #   def plus
# #       if @val
# #           empty_stack_error unless @stack.length >= 1
# #         @val += @stack.pop
# #       else
# #           empty_stack_error unless @stack.length >= 2
# #
# #         num2 = @stack.pop
# #         num1 = @stack.pop
# #         @val = num1 + num2
# #       end
# #   end
# #
# #   def minus
# #
# #     if @val
# #         empty_stack_error unless @stack.length >= 1
# #
# #       @val -= @stack.pop
# #     else
# #         empty_stack_error unless @stack.length >= 2
# #
# #       num2 = @stack.pop
# #       num1 = @stack.pop
# #
# #       @val = num1 - num2
# #     end
# #   end
# #
# #   def times
# #
# #     if @val
# #         empty_stack_error unless @stack.length >= 1
# #
# #       @val *= @stack.pop
# #     else
# #         empty_stack_error unless @stack.length >= 2
# #
# #       num2 = @stack.pop
# #       num1 = @stack.pop
# #
# #       @val = num1.to_f * num2.to_f
# #     end
# #   end
# #
# #   def divide
# #
# #     if @val
# #         empty_stack_error unless @stack.length >= 1
# #         @val /= @stack.pop
# #     else
# #         empty_stack_error unless @stack.length >= 2
# #
# #       num2 = @stack.pop
# #       num1 = @stack.pop
# #
# #       @val = num1.to_f / num2.to_f
# #     end
# #   end
# #
# #   def value
# #       @val
# #   end
# #
# #   def tokens(numbers)
# #     tokens = []
# #     symbols = "/+*-"
# #     numbers.chars.each do |num|
# #       next if num == " "
# #       if symbols.include?(num)
# #         tokens << num.to_sym
# #       else
# #         tokens << num.to_i
# #       end
# #     end
# #   tokens
# #   end
# #
# #
# #   def evaluate(numbers)
# #     new_eq = 0
# #     usable_ops = tokens(numbers)
# #     usable_ops =
# #
# #   end
# # #   it "evaluates a string" do
# # #     expect(calculator.evaluate("1 2 3 * +")).to eq(
# # #       ((2 * 3) + 1)
# # #     )
# # #
# # #     expect(calculator.evaluate("4 5 -")).to eq(
# # #       (4 - 5)
# # #     )
# # #
# # #     expect(calculator.evaluate("2 3 /")).to eq(
# # #       (2.0 / 3.0)
# # #     )
# # #
# # #     expect(calculator.evaluate("1 2 3 * + 4 5 - /")).to eq(
# # #       (1.0 + (2 * 3)) / (4 - 5)
# # #     )
# # #   end
# # # end
# #
# #   private
# #   def empty_stack_error
# #       raise "calculator is empty"
# #   end
# #
# # end
# # require 'byebug'
# class RPNCalculator
#   attr_accessor :calculator
#
#   def initialize
#     @stack = []
#     @length = 0
#   end
#
#   def push(num)
#     @stack.push(num)
#     @length += 1
#   end
#
#   def plus
#     self.checker
#     popped = @stack.pop
#     @stack[-1] += popped
#     @length -= 1
#   end
#
#   def minus
#     self.checker
#     popped = @stack.pop
#     @stack[-1] -= popped
#     @length -= 1
#   end
#
#   def times
#     self.checker
#     popped = @stack.pop
#     @stack[-1] *= popped
#     @length -= 1
#   end
#
#   def divide
#     self.checker
#     popped = @stack.pop
#     @stack[-1] = @stack[-1].fdiv(popped)
#     @length -= 1
#   end
#
#   def value
#     @stack[-1]
#   end
#
#   def tokens(tokens)
#     operations = ["+","-","*","/"]
#     tokens.split.map do |token|
#       if operations.include?(token)
#         token.to_sym
#       else
#         token.to_i
#       end
#     end
#   end
#
#   def evaluate(str)
#     # debugger
#     self.tokens(str).each do |item|
#       if item.class == "Fixnum"
#         self.push(item)
#       elsif item == :+
#         self.plus
#       elsif item == :-
#         self.minus
#       elsif item == :*
#         self.times
#       elsif item == :/
#         self.divide
#       end
#     end
#     self.value
#   end
#
#   def checker
#     if @length <= 1
#       raise "calculator is empty"
#     end
#   end
# end
